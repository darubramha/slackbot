### What's this?

This is a ChangeTip-like micropayment bot that integrates well in Slack communities.

### Run

Join the test slack first with this link: https://join.slack.com/indiumtesting/shared_invite/MjA1MzA4MjYzMDkyLTE0OTg2ODk2OTYtZGI0NzM0ZDBjMw

```
# Install PostgreSQL and create a database called indium_development. Specific the credentials in ./launchdev file
# Get correct values of SLACK_CLIENT_SECRET and INDIUM_TOKEN from us and put them in ./launchdev
bundle install
./launchdev
```
Now ping @indiumtestingbot in the testing Slack.

For console: `bin/rails`
For clearing all tables: `./dberase`
For running all migrations: `./dbup` or `./launchdev`
