require_relative 'commands/help'
require_relative 'commands/whoami'
require_relative 'commands/pay'
require_relative 'commands/balance'
require_relative 'commands/history'
require_relative 'commands/bonus'
require_relative 'commands/eval'
require_relative 'commands/chain'
require_relative 'commands/togglegif'
require_relative 'commands/stats'
require_relative 'commands/echo'

class String
  def with_env
    ((ENV['RACK_ENV'].to_s == "production") ? "" : (ENV['RACK_ENV'].to_s + ": ")) + self.to_s
  end

  def indium_account
    Account.from_id(self)
  end
end
