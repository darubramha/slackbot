ENV['RACK_ENV'] ||= 'development'
require 'bundler'
Bundler.require :default

require_relative 'commands'
require_relative 'constants'
require_relative 'models'
require 'yaml'
require 'irb'
#ActiveRecord::Base.establish_connection(YAML.load_file('config/postgresql.yml')[ENV['RACK_ENV']])
ActiveRecord::Base.establish_connection(ENV['DATABASE_URL'] || ("postgres://localhost/indium_" + ENV['RACK_ENV']) )

=begin
if ENV['RACK_ENV'] == 'development'
  puts 'Loading NewRelic in developer mode ...'
  require 'new_relic/rack/developer_mode'
  use NewRelic::Rack::DeveloperMode
end

NewRelic::Agent.manual_start
=end

SlackRubyBotServer::App.instance.prepare!
SlackRubyBotServer::Service.start!
#binding.irb
run SlackRubyBotServer::Api::Middleware.instance
