class Pay < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "PAY: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"

    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    sender = Account.new(platform: platform, team: team_id, member: user_id)
    #binding.irb

    if sender.invalid?
      logger.info "PAY: platform = #{platform}, team_id = #{team_id}, user_id = #{user_id}, message = #{data.text}"
      client.say(channel: data.channel, text: "Sender account INVALID: #{sender.errors.inspect}".with_env)
      return
    elsif /\A<@([^\s]+)> (\d+)\z/.match(_match['expression'])
      receiver, amount = /\A<@([^\s]+)> (\d+)\z/.match(_match['expression']).captures
      logger.info "p1: receiver: #{receiver} amount: #{amount}"
      receiver_account = Account.new(platform: platform, team: team_id, member: receiver)
      #client.say(channel: data.channel, text: "You want to pay #{receiver} #{amount}?")
    elsif /\A(\d+) (?:TO)? <@([^\s]+)>\z/.match(_match['expression'])
      amount, receiver = /\A(\d+) <@([^\s]+)>\z/.match(_match['expression']).captures
      logger.info "p2: receiver: #{receiver} amount: #{amount}"
      receiver_account = Account.new(platform: platform, team: team_id, member: receiver)
      #client.say(channel: data.channel, text: "You want to pay #{receiver} #{amount}?")
    elsif /\Aslack\|[A-Z0-9]+\|[A-Z0-9]+ \d+\z/.match(_match['expression'])
      # Transfer across slack teams
      receiver_team, receiver_user, amount = /\Aslack\|([A-Z0-9]+)\|([A-Z0-9]+) (\d+)\z/.match(_match['expression']).captures
      logger.info "p3: receiver_team: #{receiver_team} receiver_user: #{receiver_user} amount: #{amount}"
      receiver_account = Account.new(platform: "slack", team: receiver_team, member: receiver_user)
    else
      client.say(channel: data.channel, text: "Sorry, I couldn't understand that. Use `pay @receiver <amount>`. Try `help` for other commands.".with_env)
      return
    end

    txn, message = Txn.pay(sender, receiver_account, amount.to_i, metadata = {comments: "Transfer"}, via: "slackbot")
    if txn
      message = "You have transferred #{amount.to_i} to #{receiver_account.slack_display_name(team_id)} successfully. Your new balance is #{txn.sender_new_balance}."
      client.say(channel: data.channel, text: message.with_env)
    else
      client.say(channel: data.channel, text: message.with_env)
    end
  end
end
