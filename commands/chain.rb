class Chain < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "CHAIN: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"
    #binding.irb
    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    account = Account.new(platform: platform, team: team_id, member: user_id)

    if account.valid?
      txns = Txn.order(id: :desc).limit(20)
      if txns.blank?
        msg = "No transactions found."
      else
        msg = txns.collect { |t| "ID: #{t.id}, sender: #{Account.from_id(t.sender).slack_display_name(account.team)}, receiver: #{Account.from_id(t.receiver).slack_display_name(account.team)}, amount: #{t.amount}, timestamp: #{t.created_at}, metadata: #{t.metadata.to_s}, via: #{t.via}" }.join("\n")
      end
      client.say(channel: data.channel, text: msg.with_env)
    else
      logger.info "CHAIN: platform = #{platform}, team_id = #{team_id}, user_id = #{user_id}, message = #{data.text}"
      client.say(channel: data.channel, text: "Account invalid: #{account.errors.inspect}".with_env)
    end
  end
end
