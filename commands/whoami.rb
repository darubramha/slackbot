class Whoami < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "WHOAMI: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"
    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    account = Account.new(platform: platform, team: team_id, member: user_id)

    text = "User name: <@#{user_id}>, User ID: `#{data.user}`, Team Name: #{team_name}, Team ID: `#{team_id}`"
    if account.valid?
      msg = "Account ID: `#{account.id}` is VALID"
    else
      msg = "Account ID: `#{account.id}` is INVALID: #{account.errors.inspect}"
    end

    client.say(channel: data.channel, text: (text + "\n" + msg).with_env)
  end
end
