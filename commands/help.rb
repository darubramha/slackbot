class Help < SlackRubyBot::Commands::Base
  HELP = <<-EOS.freeze
```
The following commands are supported:

pay        - make a transfer (pay @receiver <amount>)
bonus      - claim your joining bonus! (only for early users, so claim it fast!)
balance    - check your account balance
history    - check your last 10 transactions
stats      - check various metrics and prominent account balances
chain      - see global 20 recent transactions
whoami     - print your Slack user information
help       - see this helpful message

```
NEWS: 

First 100 people joining Indium devs' slack will be given 2000 coins as joining bonus. If you want more coins, please help with the development! You can join the slack here: https://join.slack.com/indiumtalk/shared_invite/MTk4MDQwNzMwMDIzLTE0OTczODIwNDItMzBkY2RlMjk0Yg

To join "Sawaal", our tip-based Q&A slack, go here: https://join.slack.com/sawaal/shared_invite/MjA2MDQzNzY3NjA3LTE0OTg2ODM5MjMtNTMwYjg5Zjg2Mw

Slackbot source code is available here: https://gitlab.com/indium/slackbot

Homepage: http://indium.org.in/

Database dump is currently only on a HTML page. Need help in improving this: https://slackbot.indium.org.in/list
EOS
# echo               - useful for debugging while implementing commands
# eval               - remote debugging. Requires the secret keyphrase
# togglegif          - enable/disable GIFs: Only admin can run
  def self.call(client, data, _match)
    client.say(channel: data.channel, text: HELP.with_env)
    #client.say(channel: data.channel, gif: 'help')
    logger.info "HELP: #{client.owner}, user=#{data.user}"
  end
end
