class Echo < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    message = "ECHO: client.owner = #{client.owner}, user=#{data.user}, message=#{data.text}, match=#{_match.inspect}"
    client.say(channel: data.channel, text: message.with_env)
  end
end
