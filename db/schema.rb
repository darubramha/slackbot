# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170606210029) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "teams", force: :cascade do |t|
    t.string   "team_id",                                       null: false
    t.string   "name",                                          null: false
    t.boolean  "active",                        default: true,  null: false
    t.string   "domain"
    t.string   "token"
    t.boolean  "gif_enabled",                   default: false, null: false
    t.integer  "join_bonus",                    default: 0,     null: false
    t.integer  "max_user_join_bonus_count",     default: 0,     null: false
    t.integer  "actual_user_join_bonus_count",  default: 0,     null: false
    t.integer  "max_user_join_bonus_amount",    default: 0,     null: false
    t.integer  "actual_user_join_bonus_amount", default: 0,     null: false
    t.integer  "max_admin_reward",              default: 0,     null: false
    t.integer  "actual_admin_reward",           default: 0,     null: false
    t.integer  "max_introducer_reward",         default: 0,     null: false
    t.integer  "actual_introducer_reward",      default: 0,     null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "txns", force: :cascade do |t|
    t.string   "sender",               null: false
    t.string   "receiver",             null: false
    t.integer  "amount",               null: false
    t.integer  "sender_new_balance",   null: false
    t.integer  "receiver_new_balance", null: false
    t.json     "metadata"
    t.string   "via"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

end
