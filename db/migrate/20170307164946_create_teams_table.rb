require_relative '../../constants'

class CreateTeamsTable < ActiveRecord::Migration[5.0]
  class Team < ActiveRecord::Base; end
  def self.up
    create_table :teams, force: true do |t|
      t.string :team_id, null: false
      t.string :name, null: false
      t.boolean :active, null: false, default: true
      t.string :domain
      t.string :token
      t.boolean :gif_enabled, null: false, default: false

      t.integer :join_bonus, null: false, default: 0 # per-user

      t.integer :max_user_join_bonus_count, null: false, default: 0
      t.integer :actual_user_join_bonus_count, null: false, default: 0

      t.integer :max_user_join_bonus_amount, null: false, default: 0
      t.integer :actual_user_join_bonus_amount, null: false, default: 0

      t.integer :max_admin_reward, null: false, default: 0
      t.integer :actual_admin_reward, null: false, default: 0 # how much has already been given to admin

      t.integer :max_introducer_reward, null: false, default: 0
      t.integer :actual_introducer_reward, null: false, default: 0

      t.timestamps
    end

    unless ENV['INDIUM_TOKEN'].blank?
      Team.create(
        team_id: SLACK_TEAMS[:indium][:id],
        name: SLACK_TEAMS[:indium][:name],
        active: true,
        token: ENV['INDIUM_TOKEN'],
        gif_enabled: false,
        join_bonus: (2*SLACK_USER_JOIN_REWARD),
        max_user_join_bonus_count: 100, # for now, give bonus to only first 100 users in this slack team
        max_user_join_bonus_amount: (100 * 2 * SLACK_USER_JOIN_REWARD)
      )

      Team.create(
        team_id: SLACK_TEAMS[:printsend][:id], # they contributed a month's worth of dev and testing effort before launch
        name: SLACK_TEAMS[:printsend][:name],
        active: false,
        gif_enabled: false,
        join_bonus: (5 * SLACK_USER_JOIN_REWARD),
        max_user_join_bonus_count: 10, # only 10 users can claim bonus from this team
        max_user_join_bonus_amount: (10 * 5 * SLACK_USER_JOIN_REWARD),
        max_admin_reward: 0, # they will be paid 300K coins out of @dev, not @slackadminpromo
        max_introducer_reward: 0 # no reward, as they are invited for technical contribution, not as early adopters
      )

      Team.create(
        team_id: SLACK_TEAMS[:sawaal][:id],
        name: SLACK_TEAMS[:sawaal][:name],
        active: false,
        gif_enabled: false,
        join_bonus: (1 * SLACK_USER_JOIN_REWARD),
        max_user_join_bonus_count: 100, # only 100 users can claim bonus from this team
        max_user_join_bonus_amount: (100 * 1 * SLACK_USER_JOIN_REWARD),
        max_admin_reward: 200_000, # reward admin from @advocacy for launching and growing the community
        max_introducer_reward: 0 # no reward, as it was created by us
      )
    end
  end

  def self.down
    drop_table :teams
  end
end
